#!/usr/bin/env bash

sudo apt-get --yes update

# Essentials
sudo apt-get install --yes build-essential
sudo apt-get install --yes curl
sudo apt-get install --yes wget
sudo apt-get install --yes git

# Java
sudo add-apt-repository ppa:webupd8team/java --yes
sudo apt-get --yes update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get install oracle-java8-installer --yes
sudo apt-get install oracle-java8-set-default --yes
