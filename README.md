# Sendtrix API Workspace
This is a virtual environment for development. It contains all the tools required to code the Sendtrix API.

## Requirements
- Vagrant
- VirtualBox

## Launch
Clone this repository to a folder in your machine.

```console
> git clone git:your-username@repository-name.repository-host.com
```

Run the following instructions in the command line in the same folder where the files are stored:

```console
> vagrant up
```

## Access to the machine
Run the following instructions in the command line in the same folder where the files are stored:

```console
> vagrant ssh
```
Then you would be able to connect to the machine where is running
